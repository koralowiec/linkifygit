/* eslint-disable no-undef */
const id = "link-to-git-repo";
const title = "Open page of git repo in New Tab";
const menuElement = {
  id,
  title,
  contexts: ["all"],
};

browser.contextMenus.create(menuElement);
browser.contextMenus.onClicked.addListener((info) => {
  if (info.menuItemId === id) {
    const potentialSshGitLink = info.selectionText;
    const begining = "git@";
    const ending = ".git";
    if (
      potentialSshGitLink.startsWith(begining) &&
      potentialSshGitLink.endsWith(ending)
    ) {
      const lenOfLink = potentialSshGitLink.length - ending.length;
      let link = potentialSshGitLink.substring(begining.length, lenOfLink);
      link = link.replace(":", "/");
      link = `https://${link}`;
      const tab = {
        url: link,
      };
      browser.tabs.create(tab);
    }
  }
});
