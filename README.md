# LinkfyGit

It's just a simple web plugin, which adds an option to Context Menu (right
click menu) for opening a page of git repo for selected ssh like git address
(eg. for git@gitlab.com:gitlab-org/gitlab.git will open
https://gitlab.com/gitlab-org/gitlab in new tab).

![Demo](./demo.gif)
